package net.therap.todomanager.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = {"net.therap.todomanager"},
        excludeFilters = {@ComponentScan.Filter(type = FilterType.REGEX, pattern = {"net.therap.todomanager.web.*"})})
public class RootConfig {

}