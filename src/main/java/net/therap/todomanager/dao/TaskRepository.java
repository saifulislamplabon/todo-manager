package net.therap.todomanager.dao;

import net.therap.todomanager.domain.Task;

import java.util.List;

/**
 * @author saiful
 * @since 5/9/17
 */
public interface TaskRepository {

    public Task save(Task task);

    public Task find(long id);

    public void delete(Task task);

    public void delete(long id);

    public List<Task> findAll();
}
