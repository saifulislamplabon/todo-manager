package net.therap.todomanager.dao;

import net.therap.todomanager.domain.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author saiful
 * @since 5/9/17
 */
@Repository
public class TaskRepositoryImpl implements TaskRepository {

    private static final String FIND_ALL_JPQL = "SELECT task FROM Task task";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public Task save(Task task) {
        if (task.getTaskId() != 0) {
            entityManager.merge(task);
        } else {
            entityManager.persist(task);
        }
        return task;
    }

    @Override
    public Task find(long id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    @Transactional
    public void delete(Task task) {
        if (entityManager.contains(task)) {
            entityManager.remove(task);
        } else {
            delete(task.getTaskId());
        }
    }

    @Override
    @Transactional
    public void delete(long id) {
        entityManager.remove(find(id));
    }

    @Override
    public List<Task> findAll() {
        return entityManager.createQuery(FIND_ALL_JPQL, Task.class).getResultList();
    }
}
