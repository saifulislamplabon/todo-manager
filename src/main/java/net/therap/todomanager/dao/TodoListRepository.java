package net.therap.todomanager.dao;

import net.therap.todomanager.domain.TodoList;

import java.util.List;

/**
 * @author saiful
 * @since 5/11/17
 */
public interface TodoListRepository {

    public TodoList save(TodoList todoList);
    public TodoList find(long listId);
    public TodoList findWithTask(long listId);
    public void delete(TodoList todoList);
    public void delete(long listId);
    public List<TodoList> findAll();
}
