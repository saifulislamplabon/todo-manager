package net.therap.todomanager.dao;

import net.therap.todomanager.domain.TodoList;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author saiful
 * @since 5/11/17
 */
@Repository
public class TodoListRepositoryImpl implements TodoListRepository {

    private static final String FIND_ALL_JPQL = "SELECT DISTINCT todoList FROM TodoList todoList LEFT JOIN FETCH todoList.tasks";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public TodoList save(TodoList todoList) {
        if (todoList.getListId() != 0) {
            entityManager.merge(todoList);
        } else {
            entityManager.persist(todoList);
        }
        return todoList;
    }

    @Override
    public TodoList find(long listId) {
        return entityManager.find(TodoList.class, listId);
    }

    @Override
    public TodoList findWithTask(long listId) {
        String query = FIND_ALL_JPQL + " WHERE todoList.listId=:listId";
        TypedQuery<TodoList> todoListTypedQuery = entityManager.createQuery(query, TodoList.class);
        todoListTypedQuery.setParameter("listId", listId);
        return todoListTypedQuery.getSingleResult();
    }

    @Override
    @Transactional
    public void delete(TodoList todoList) {
        if (entityManager.contains(todoList)) {
            entityManager.remove(todoList);
        } else {
            delete(todoList.getListId());
        }
    }

    @Override
    @Transactional
    public void delete(long listId) {
        entityManager.remove(find(listId));
    }

    @Override
    public List<TodoList> findAll() {
        return entityManager.createQuery(FIND_ALL_JPQL, TodoList.class).getResultList();
    }
}
