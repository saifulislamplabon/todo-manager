package net.therap.todomanager.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author saiful
 * @since 5/9/17
 */
@Entity
@Table(name = "task")
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "task_id")
    private long taskId;

    @NotNull
    @Size(max = 255, message = "Task description must be less than 255 characters")
    @Column(name = "task_description")
    private String taskDescription;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "task_status")
    private TaskStatus taskStatus;

    @ManyToOne(cascade = {CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST})
    @JoinTable(name = "todolist_task", inverseJoinColumns =@JoinColumn(name = "list_id"),
            joinColumns = @JoinColumn(name = "task_id"))
    private TodoList todoList;

    public Task() {
    }

    public Task(String taskDescription, TaskStatus taskStatus) {
        this.taskDescription = taskDescription;
        this.taskStatus = taskStatus;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public TodoList getTodoList() {
        return todoList;
    }

    public void setTodoList(TodoList todoList) {
        this.todoList = todoList;
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskId=" + taskId +
                ", taskDescription='" + taskDescription + '\'' +
                ", taskStatus=" + taskStatus +
                '}';
    }
}
