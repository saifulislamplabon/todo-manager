package net.therap.todomanager.domain;

/**
 * @author saiful
 * @since 5/9/17
 */
public enum TaskStatus {
    TODO("Done"), DONE("Undone");

    private final String actionName;

    TaskStatus(String actionName) {
        this.actionName = actionName;
    }

    public String getActionName() {
        return actionName;
    }
}
