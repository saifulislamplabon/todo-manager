package net.therap.todomanager.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author saiful
 * @since 5/11/17
 */
@Entity
@Table(name = "todolist")
public class TodoList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "list_id")
    private long listId;

    @Column(name = "list_name")
    @Size(min = 3, max = 128, message = "List name must be between 3 and 128 characters")
    private String listName;


    @OneToMany(cascade = {CascadeType.MERGE,
            CascadeType.REFRESH,
            CascadeType.PERSIST},
            targetEntity = Task.class)
    @JoinTable(name = "todolist_task", joinColumns = @JoinColumn(name = "list_id"),
            inverseJoinColumns = @JoinColumn(name = "task_id"))
    private List<Task> tasks = new ArrayList<>();

    public TodoList() {
    }

    public TodoList(String listName) {
        this.listName = listName;
    }

    public long getListId() {
        return listId;
    }

    public void setListId(long listId) {
        this.listId = listId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String toString() {
        return "TodoList{" +
                "listId=" + listId +
                ", listName='" + listName + '\'' +
                ", tasks=" + tasks +
                '}';
    }
}
