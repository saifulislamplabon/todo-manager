package net.therap.todomanager.service;

import net.therap.todomanager.dao.TaskRepository;
import net.therap.todomanager.domain.Task;
import net.therap.todomanager.domain.TaskStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author saiful
 * @since 5/12/17
 */

@Component
public class TaskService {

    private TaskRepository taskRepository;
    private Logger logger = LoggerFactory.getLogger(TaskService.class);

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void markTaskAsDone(long taskId) {
        Task task = taskRepository.find(taskId);
        task.setTaskStatus(TaskStatus.DONE);
        task = taskRepository.save(task);
        logger.debug("Task : {}", task);
    }

    public void markTaskAsUndone(long taskId) {
        Task task = taskRepository.find(taskId);
        task.setTaskStatus(TaskStatus.TODO);
        task = taskRepository.save(task);
        logger.debug("Task : {}", task);
    }

}
