package net.therap.todomanager.service;

import net.therap.todomanager.dao.TaskRepository;
import net.therap.todomanager.dao.TodoListRepository;
import net.therap.todomanager.domain.Task;
import net.therap.todomanager.domain.TaskStatus;
import net.therap.todomanager.domain.TodoList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author saiful
 * @since 5/11/17
 */
@Component
public class TodoListService {

    private TaskRepository taskRepository;
    private TodoListRepository todoListRepository;

    @Autowired
    public TodoListService(TaskRepository taskRepository, TodoListRepository todoListRepository) {
        this.taskRepository = taskRepository;
        this.todoListRepository = todoListRepository;
    }

    public void addTaskToList(Task task, long listId){
        task.setTaskStatus(TaskStatus.TODO); //Since this method is supposed to add new task to the list
        TodoList todoList = todoListRepository.findWithTask(listId);
        todoList.getTasks().add(task);
        todoListRepository.save(todoList);
    }
}
