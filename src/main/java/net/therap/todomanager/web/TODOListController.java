package net.therap.todomanager.web;

import net.therap.todomanager.dao.TodoListRepository;
import net.therap.todomanager.domain.Task;
import net.therap.todomanager.domain.TodoList;
import net.therap.todomanager.service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author saiful
 * @since 5/9/17
 */
@Controller
@RequestMapping(value = {"/", "/todolists"})
public class TODOListController {

    private TodoListRepository todoListRepository;
    private TodoListService todoListService;

    @Autowired
    public TODOListController(TodoListRepository todoListRepository, TodoListService todoListService) {
        this.todoListRepository = todoListRepository;
        this.todoListService = todoListService;
    }

    @RequestMapping(method = GET)
    public String home(Model model) {
        model.addAttribute(todoListRepository.findAll());
        model.addAttribute(new TodoList());
        model.addAttribute(new Task());
        return "home";
    }

    @RequestMapping(method = POST)
    public String addTODOList(@Valid TodoList todoList, Errors errors, Model model) {
        if (errors.hasErrors()) {
            model.addAttribute(todoListRepository.findAll());
            model.addAttribute(new Task());
            return "home";
        }
        todoListRepository.save(todoList);
        System.out.println(todoList.getListName());
        return "redirect:/";
    }

    @RequestMapping(value = "/delete/{listId}", method = POST)
    public String removeTodoList(@PathVariable long listId) {
        todoListRepository.delete(listId);
        return "redirect:/";
    }
}
