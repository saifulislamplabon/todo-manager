package net.therap.todomanager.web;

import net.therap.todomanager.dao.TaskRepository;
import net.therap.todomanager.domain.Task;
import net.therap.todomanager.service.TaskService;
import net.therap.todomanager.service.TodoListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author saiful
 * @since 5/12/17
 */
@Controller
@RequestMapping(value = "/tasks")
public class TaskController {

    private TaskRepository taskRepository;
    private TaskService taskService;
    private TodoListService todoListService;

    @Autowired
    public TaskController(TaskRepository taskRepository, TaskService taskService,
                          TodoListService todoListService) {
        this.taskRepository = taskRepository;
        this.taskService = taskService;
        this.todoListService = todoListService;
    }

    @RequestMapping(value = "/addTaskToList", method = POST)
    public String addTaskToList(@RequestParam long listId, @Valid Task task, Errors errors) {
        todoListService.addTaskToList(task, listId);
        return "redirect:/";
    }

    @RequestMapping(value = "/deleteTask/{taskId}", method = POST)
    public String removeTask(@PathVariable long taskId) {
        taskRepository.delete(taskId);
        return "redirect:/";
    }

    @RequestMapping(value = "/Done/{taskId}", method = POST)
    public String markTaskDone(@PathVariable long taskId) {
        taskService.markTaskAsDone(taskId);
        return "redirect:/";
    }

    @RequestMapping(value = "/Undone/{taskId}", method = POST)
    public String markTaskUndone(@PathVariable long taskId) {
        taskService.markTaskAsUndone(taskId);
        return "redirect:/";
    }
}
