<%@ taglib  uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%--
  Created by IntelliJ IDEA.
  User: saiful
  Date: 5/10/17
  Time: 3:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="/resources/css/main.css">
        <link rel="stylesheet" href="/resources/css/card-style.css">
        <title><decorator:title default="Therap (BD) TODO Manager"/></title>
        <decorator:head/>
    </head>
    <body>
        <div class="container">
            <div class="row page-header" id="page-header">
                <div class="col-sm-3">
                    <a href="/">
                        <img src="/resources/images/therap_logo.png">
                    </a>
                </div>
                <div class="col-sm-9 text-right">
                    <h1>Therap (BD) TODO Manager</h1>
                </div>
            </div>

            <div class="row">
                <decorator:body/>
            </div>

            <hr>
            <div class="row" id="footer">
                <br/>
                <b>Address: </b> House 47, Road 4, Block C. Dhaka 1213, Bangladesh <br>
                <b>Phone: </b> +88 02 8822766, +88 02 9821702-5 <br> <br>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>