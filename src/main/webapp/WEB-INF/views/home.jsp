<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: plabon-work
  Date: 5/9/17
  Time: 9:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Therap (BD) TODO Manager</title>
</head>
<body>

<div class="row">
    <c:forEach items="${todoListList}" var="list">
        <div class="col-md-4 col-sm-6">
            <div class="card">
                <span class="card-title">${list.listName}</span>

                <div class="card-content">
                    <c:forEach items="${list.tasks}" var="task">
                        <c:if test="${task.taskStatus.actionName =='Undone'}">
                            <c:set value="mark-done" var="taskClass"></c:set>
                            <c:set value="btn btn-sm btn-block btn-warning" var="taskButtonClass"></c:set>
                        </c:if>
                        <c:if test="${task.taskStatus.actionName =='Done'}">
                            <c:set value="mark-undone" var="taskClass"></c:set>
                            <c:set value="btn btn-sm btn-block btn-success" var="taskButtonClass"></c:set>
                        </c:if>

                        <div class="row">
                            <div class="col-sm-6 ${taskClass}">
                                <c:out value="${task.taskDescription}"/>
                            </div>
                            <div class="col-sm-3">
                                <sf:form action="/tasks/${task.taskStatus.actionName}/${task.taskId}">
                                    <input class="${taskButtonClass}" type="submit"
                                           value="${task.taskStatus.actionName}">
                                </sf:form>
                            </div>
                            <div class="col-sm-3">
                                <sf:form action="/tasks/deleteTask/${task.taskId}">
                                    <input class="btn btn-danger btn-sm" type="submit" value="Remove">
                                </sf:form>
                            </div>
                        </div>
                    </c:forEach>
                    <sf:form action="/tasks/addTaskToList" cssClass="form-inline" commandName="task">
                        <sf:input path="taskDescription"/>
                        <input type="hidden" name="listId" value="${list.listId}">
                        <input class="btn btn-primary" type="submit" value="Add Task"/>
                    </sf:form>

                    <sf:form action="/delete/${list.listId}">
                        <input class="btn btn-danger" type="submit" value="Delete List">
                    </sf:form>
                </div>
            </div>
        </div>
    </c:forEach>
    <div class="col-md-4">
        <div class="card">
            <div class="card-title">
                <div class="card-content">
                    <sf:form cssClass="form-inline" commandName="todoList">
                        <sf:input path="listName"/> <sf:errors path="listName" cssClass="error"/><br/><br/>
                        <input class="btn btn-primary" type="submit" value="Create New List"/>
                    </sf:form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>
