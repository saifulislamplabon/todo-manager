package net.therap.todomanager.dao;

import junit.framework.TestCase;
import net.therap.todomanager.config.RootConfig;
import net.therap.todomanager.domain.Task;
import net.therap.todomanager.domain.TaskStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RootConfig.class)
public class TaskRepositoryTest extends TestCase {

    @Autowired
    TaskRepository repository;

    @Test
    public void testSave() throws Exception {
        Task task = new Task("Test save task", TaskStatus.TODO);
        Task retrived = repository.save(task);
        assertEquals(task.getTaskDescription(), retrived.getTaskDescription());
    }

    @Test
    public void testUpdate() throws Exception {
        Task task = repository.find(1);
        task.setTaskStatus(TaskStatus.DONE);
        Task retrived = repository.save(task);
        assertEquals(TaskStatus.DONE, retrived.getTaskStatus());
    }
}