package net.therap.todomanager.dao;

import junit.framework.TestCase;
import net.therap.todomanager.config.RootConfig;
import net.therap.todomanager.domain.Task;
import net.therap.todomanager.domain.TaskStatus;
import net.therap.todomanager.domain.TodoList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RootConfig.class)
public class TodoListRepositoryTest extends TestCase {

    @Autowired
    TodoListRepository todoListRepository;
    @Autowired
    TaskRepository taskRepository;

    @Test
    public void testSaveAndUpdate() throws Exception {
        TodoList todoList = new TodoList("Work");
        todoList = todoListRepository.save(todoList);
        assertFalse(todoList.getListId()==0);
        todoList.setListName("Project");
        todoList = todoListRepository.save(todoList);
        todoList = todoListRepository.find(todoList.getListId());
        assertEquals("Project",todoList.getListName());
    }

    @Test
    public void testAddingTask() throws Exception{
        TodoList todoList = new TodoList("Project");
        todoList = todoListRepository.save(todoList);

        Task task = new Task("Hello", TaskStatus.DONE);
        task = taskRepository.save(task);

        todoList.getTasks().add(task);

        todoList = todoListRepository.save(todoList);

        taskRepository.delete(task);

    }
}